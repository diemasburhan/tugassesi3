import 'package:flutter/material.dart';
import 'image_list.dart';

class ImageCard extends StatefulWidget {
  const ImageCard({super.key});

  @override
  State<ImageCard> createState() => _ImageCardState();
}

class _ImageCardState extends State<ImageCard> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Image.asset('assets/images/foto_profile.jpg'),
        CircleAvatar(
          backgroundImage: AssetImage('assets/images/foto_profile.jpg'),
        ),
        Text('Ruby Ilham Fadilah')
      ],
    );
  }
}
